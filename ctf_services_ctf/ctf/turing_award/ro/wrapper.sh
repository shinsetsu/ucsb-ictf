#!/bin/bash

cd /opt/ctf/turing_award/rw

if [[ "i386" == "x86_64" ]] || [[ "x86_64" == "x86_64" ]] ; then
  ../ro/turing_award 2>/dev/null
else
  qemu-x86_64 ../ro/turing_award 2>/dev/null
fi