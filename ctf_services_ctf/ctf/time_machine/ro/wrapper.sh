#!/bin/bash

cd /opt/ctf/time_machine/rw

if [[ "i386" == "x86_64" ]] || [[ "x86_64" == "x86_64" ]] ; then
  ../ro/time_machine 2>/dev/null
else
  qemu-x86_64 ../ro/time_machine 2>/dev/null
fi