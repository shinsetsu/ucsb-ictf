#!/bin/bash

cd /opt/ctf/cybercrime64k/rw

if [[ "i386" == "x86_64" ]] || [[ "x86_64" == "x86_64" ]] ; then
  ../ro/cybercrime64k 2>/dev/null
else
  qemu-x86_64 ../ro/cybercrime64k 2>/dev/null
fi