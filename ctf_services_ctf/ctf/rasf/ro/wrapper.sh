#!/bin/bash

cd /opt/ctf/rasf/rw

if [[ "i386" == "x86_64" ]] || [[ "x86_64" == "x86_64" ]] ; then
  ../ro/rasf 2>/dev/null
else
  qemu-x86_64 ../ro/rasf 2>/dev/null
fi