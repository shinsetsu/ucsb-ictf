#!/bin/bash

cd /opt/ctf/rocketscience/rw

if [[ "i386" == "x86_64" ]] || [[ "x86_64" == "x86_64" ]] ; then
  ../ro/rocketscience 2>/dev/null
else
  qemu-x86_64 ../ro/rocketscience 2>/dev/null
fi